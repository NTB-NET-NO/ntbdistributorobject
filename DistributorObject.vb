Imports System.Configuration.ConfigurationSettings
Imports System.IO
Imports System.text
Imports System.Text.RegularExpressions
Imports System.xml

Imports CdoHelper
Imports Redemption

Public Class DistributorObject
    Inherits System.ComponentModel.Component

#Region " Component Designer generated code "

    Public Sub New(ByVal Container As System.ComponentModel.IContainer)
        MyClass.New()

        'Required for Windows.Forms Class Composition Designer support
        Container.Add(Me)
    End Sub

    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Component overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.
    'Do not modify it using the code editor.
    Friend WithEvents MessagePollTimer As System.Timers.Timer
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.MessagePollTimer = New System.Timers.Timer
        CType(Me.MessagePollTimer, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'MessagePollTimer
        '
        Me.MessagePollTimer.Interval = 10000
        CType(Me.MessagePollTimer, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

    'Variables
    Private logFolder As String
    Private tempFolder As String
    Private removeTemp As Boolean

    Private clipDirekte As Boolean
    Private xsltDirekteClipper As String
    Private clipNPKDirekte As Boolean
    Private xsltNPKDirekteClipper As String
    Private clipTikker As Boolean
    Private xsltTikkerClipper As String

    Private rtfMode As String
    Private mailProfile As String

    Protected IPTC_Sequence As Integer
    Protected IPTC_SequenceFile As String
    Protected IPTC_Range As String

    'Objects
    Private mapiSession As MAPI.Session
    Private infoStore As MAPI.InfoStore
    Private rootFolder As MAPI.Folder

    Private folderList As XmlNodeList
    Private fieldMap As XmlNodeList

    'Busy-event
    Private _ready As Threading.AutoResetEvent = New Threading.AutoResetEvent(True)

    Private relogon As Boolean = False

    'Conversion objects
    Private objBitnames As Bitnames
    Private NITF32_Converter As WordMl2Nitf

    Private SqlConnectionString As String
    Private bitnameOfflineFile As String


    Public Function Init() As Boolean

        Try

            'Set up logging
            logFolder = AppSettings("logFolder")
            Directory.CreateDirectory(logFolder)

            WriteLog(logFolder, "NITFDistributor initiating...")

            'Temp files
            tempFolder = AppSettings("tempFolder")
            Directory.CreateDirectory(tempFolder)
            Directory.CreateDirectory(tempFolder & "\debug")
            Directory.CreateDirectory(tempFolder & "\wordml")
            Directory.CreateDirectory(tempFolder & "\rtf")

            rtfMode = UCase(AppSettings("RTFMode"))
            removeTemp = AppSettings("removeTempFiles")

            clipDirekte = LCase(AppSettings("clipDirekte")) = "yes"
            xsltDirekteClipper = AppSettings("xsltDirekteClipper")

            clipNPKDirekte = LCase(AppSettings("clipNPKDirekte")) = "yes"
            xsltNPKDirekteClipper = AppSettings("xsltNPKDirekteClipper")

            clipTikker = LCase(AppSettings("clipTikker")) = "yes"
            xsltTikkerClipper = AppSettings("xsltTikkerClipper")

            IPTC_Range = AppSettings("IPTCRange")
            IPTC_SequenceFile = AppSettings("IPTCSequenceFile")

            IPTC_Sequence = CInt(IPTC_Range.Split("-")(0))
            LoadSequence(IPTC_SequenceFile, True)

            MessagePollTimer.Interval = 5000

            SqlConnectionString = AppSettings("SqlConnectionString")
            bitnameOfflineFile = AppSettings("BitnameOfflineFile")

            'Load Exchange objects
            mailProfile = AppSettings("MailProfile")

            LogonExchange()

            'Load field map
            Try
                Dim doc As XmlDocument = New XmlDocument
                doc.Load(AppSettings("fieldMapFile"))
                fieldMap = doc.SelectNodes("/notabene/field")
            Catch ex As Exception
                WriteErr(logFolder, "Failed to read Notabene field Map.", ex)
                Return False
            End Try

            'Load folders to watch
            Try
                Dim doc As XmlDocument = New XmlDocument
                doc.Load(AppSettings("folderConfig"))
                folderList = doc.SelectNodes("/folders/folder")
            Catch ex As Exception
                WriteErr(logFolder, "Failed to read folder configuration file.", ex)
                Return False
            End Try

            'Check the folderlist
            Try
                Dim node As XmlNode
                For Each node In folderList
                    Directory.CreateDirectory(node.Attributes("output").Value)
                    WriteLog(logFolder, node.Attributes("name").Value & " -> " & node.Attributes("output").Value)
                Next
            Catch ex As Exception
                WriteErr(logFolder, "Failed to read folder configuration file.", ex)
                Return False
            End Try

            'Load converters
            WriteLog(logFolder, "Loading MS Word conversion object.")
            NITF32_Converter = New WordMl2Nitf(AppSettings("xsltFile"))

            WriteLog(logFolder, "Loading bitnames table cache from database.")
            objBitnames = New Bitnames(SqlConnectionString, bitnameOfflineFile)

            WriteLog(logFolder, "NITFDistributor init complete.")

        Catch ex As Exception
            WriteErr(logFolder, "Init failed...", ex)
        End Try

        Return True
    End Function

    Private Sub LoadSequence(ByVal file As String, ByVal load As Boolean)
        Try
            If load Then
                IPTC_Sequence = ReadFile(file).Trim()
                WriteLog(logFolder, "IPTC Sequence number loaded: " & IPTC_Sequence)
            Else
                WriteFile(file, IPTC_Sequence, False)
                WriteLog(logFolder, "IPTC Sequence number saved: " & IPTC_Sequence)
            End If
        Catch ex As Exception
            WriteErr(logFolder, "Error accessing IPTC sequence number.", ex)
        End Try
    End Sub

    Private Function Get_Seq() As Integer

        'Check value
        If IPTC_Sequence > CInt(IPTC_Range.Split("-")(1)) Or _
           IPTC_Sequence < CInt(IPTC_Range.Split("-")(0)) Then
            IPTC_Sequence = CInt(IPTC_Range.Split("-")(0))
        End If

        'Return value
        Dim ret As Integer = IPTC_Sequence

        'Increment
        IPTC_Sequence += 1

        'Save
        LoadSequence(IPTC_SequenceFile, False)

        Return ret
    End Function

    Public Sub Start()
        WriteLogNoDate(logFolder, "-------------------------------------------------------------------------------------")
        WriteLog(logFolder, "NITFDistributor polling started.")
        WriteLogNoDate(logFolder, "-------------------------------------------------------------------------------------")

        'Start polling
        MessagePollTimer.Start()
    End Sub

    Public Sub Stopp()
        'Stop polling
        MessagePollTimer.Stop()

        _ready.WaitOne(20000, False)

        WriteLogNoDate(logFolder, "-------------------------------------------------------------------------------------")

        LogoffExchange()
        NITF32_Converter.Close("")

        WriteLog(logFolder, "NITFDistributor polling stopped.")
        WriteLogNoDate(logFolder, "-------------------------------------------------------------------------------------")
    End Sub

    Public Sub LogonExchange()
        Dim strPublicRootID As String

        Const PR_IPM_PUBLIC_FOLDERS_ENTRYID = &H66310102

        'Create new session
        mapiSession = New MAPI.SessionClass

        'Check profile settings an log on
        Try
            If mailProfile = "" Then
                mapiSession.Logon(, , False, False, 0, True)
            Else
                mapiSession.Logon(mailProfile, , False, True, 0, True)
            End If
            WriteLog(logFolder, "Logged on to " & mapiSession.Name)
        Catch ex As Exception
            WriteErr(logFolder, "MS Exchange logon failed.", ex)
        End Try

        'Get infostore and root folder
        Try
            'Language independent public folder fetch
            For Each infoStore In mapiSession.InfoStores
                Try
                    strPublicRootID = infoStore.Fields(PR_IPM_PUBLIC_FOLDERS_ENTRYID).Value()

                    ' Get root folder
                    rootFolder = mapiSession.GetFolder(strPublicRootID, infoStore.ID)
                    Exit For
                Catch ex As Exception
                    WriteErr(logFolder, "Root folder loop exception: " & infoStore.Name, ex)
                End Try
            Next

            WriteLog(logFolder, "Using InfoStore: " & infoStore.Name)
            WriteLog(logFolder, "Using root folder: " & rootFolder.Name)
        Catch ex As Exception
            WriteErr(logFolder, "Failed to retrieve infostore and root folder object.", ex)
        End Try

    End Sub

    Public Sub LogoffExchange()
        Try
            mapiSession.Logoff()
            WriteLog(logFolder, "Logged off MS Exchange.")
        Catch ex As Exception
            WriteErr(logFolder, "Failed to log off MS Exchange.", ex)
        End Try
    End Sub

    ''Gets infostore object by name
    'Private Function GetInfoStoreByName(ByVal strInfoStoreName As String) As MAPI.InfoStore

    '    Dim store As MAPI.InfoStore
    '    Dim stores As MAPI.InfoStores = mapiSession.InfoStores

    '    Dim i As Integer
    '    For i = 1 To stores.Count
    '        store = stores.Item(i)
    '        If store.Name = strInfoStoreName Then Return store
    '    Next

    '    'Return nothing, i.e not found
    '    GetInfoStoreByName = Nothing
    'End Function


    'Locates the specified folder below the root folder and returns the object
    Private Function GetFolderByName(ByVal strFolderName As String) As MAPI.Folder

        'Split up folder into array
        Dim arrFolders As String() = strFolderName.Split("\")

        'Temp vars
        Dim subfolders As MAPI.Folders
        Dim folder As MAPI.Folder = rootFolder

        'Iteration
        Dim f As String
        Dim i As Integer

        'Loop input folder names
        For Each f In arrFolders
            subfolders = folder.Folders

            'Check each subfolder on current level
            For i = 1 To subfolders.Count
                folder = subfolders.Item(i)
                If folder.Name.tolower = f.ToLower Then Exit For
            Next

            'Error checking
            If i > subfolders.Count Then
                WriteLog(logFolder, "Folder not found: " & strFolderName)
                Return Nothing
            End If
        Next

        'At this point we have the folder object
        Return folder
    End Function

    'Polling Exchange folders here
    Private Sub MessagePollTimer_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles MessagePollTimer.Elapsed

        'Filenames
        Dim rtfTmp As String
        Dim wmlTmp As String
        Dim xmlTmp As String
        Dim xmlOut As String

        Dim head As String
        Dim rtf As String

        Try
            'Stop timer
            MessagePollTimer.Stop()
            _ready.WaitOne()
            WriteLog(logFolder, "Inside PollTimer_Elapsed, polling stopped.")
        Catch ex As Exception
            WriteErr(logFolder, "Timer / Sync-object error.", ex)
        End Try


        Try
            'Mapi objects
            Dim f As MAPI.Folder
            Dim m_list As MAPI.Messages
            Dim m As MAPI.Message

            'Relogon
            If relogon Then
                LogonExchange()
                relogon = False
            End If

            'Iteration
            Dim i As Integer = 1
            Dim node As XmlNode

            'Loop the folder watchlist
            For Each node In folderList

                Try
                    f = GetFolderByName(node.Attributes("name").Value)
                Catch ex As Exception
                    'Error handling here
                    'Logon, then exit and retry polling at next tick
                    relogon = True
                    WriteLog(logFolder, "Lost connection to MS Exchange, retrying in " & AppSettings("errorDelay") & " seconds...")
                    WriteErr(logFolder, "Lost connection to MS Exchange.", ex)
                    _ready.Set()

                    MessagePollTimer.Interval = AppSettings("errorDelay") * 1000
                    MessagePollTimer.Start()

                    LogoffExchange()
                    Exit Sub
                End Try

                m_list = f.Messages

                'Loop messages in each folder
                While i <= m_list.Count

                    m = m_list.Item(i)
                    WriteLog(logFolder, "New message: " & m.Subject)

                    Dim ok As Boolean = True
                    Try
                        'Extract header fields
                        head = GetMessageHeader(m, f)

                        'Category stuff
                        head = objBitnames.MakeNotaben2NitfFields(head).InnerXml

                        'Make filenames
                        Dim base As String = MakeFileNameFromHead(head)
                        rtfTmp = tempFolder & "\rtf\" & base & ".rtf"
                        wmlTmp = tempFolder & "\wordml\" & base & ".xml"
                        xmlTmp = tempFolder & "\debug\" & base & ".xml"
                        xmlOut = node.Attributes("output").Value & "\" & base & ".xml"

                        WriteLog(logFolder, "Header and filenames generated for " & m.Subject)
                    Catch ex As Exception
                        ok = False
                        WriteErr(logFolder, "Failed to get message headers. Keyword: " & m.Subject, ex)
                    End Try

                    'Extract RTF - Two different options
                    Try
                        If rtfMode = "REDEMPTION" Then
                            Dim RTFRed As SafeMailItem = New SafeMailItemClass
                            RTFRed.Item = m
                            rtf = RTFRed.RTFBody()
                        ElseIf rtfMode = "CDOHELPER" Then
                            Dim RTFRed As MessageHelper = New MessageHelperClass
                            If RTFRed.HasTextRTF(m) Then
                                rtf = RTFRed.GetTextRTF(m)
                            Else
                                Throw New Exception("CDOHelper.HasTextRTF returned false, RTF missing.")
                            End If
                        Else
                            Throw New Exception("Invalid RTF-mode specified, use either REDEMPTION or CDOHELPER. Conversion aborted.")
                        End If

                        If rtf <> "" Then
                            'write tempfile RTF
                            WriteFile(rtfTmp, rtf, False)
                        Else
                            Throw New Exception("RTF-body empty. Possibly missing RTF message content.")
                        End If

                        WriteLog(logFolder, "RTF content extracted from " & m.Subject)
                    Catch ex As Exception
                        ok = False
                        WriteErr(logFolder, "Failed to get RTF text from message. Keyword: " & m.Subject, ex)
                    End Try

                    'Convert document here
                    Dim xml As String
                    If ok Then
                        Try
                            xml = NITF32_Converter.ConvertDoc(rtfTmp, head, wmlTmp, xmlTmp)
                            WriteLog(logFolder, "RTF converted to XML.")
                        Catch ex As Exception
                            ok = False
                            WriteErr(logFolder, "Failed to convert RTF to XML: " & rtfTmp, ex)

                            If ex.message = "The RPC server is too busy to complete this operation." Or _
                               ex.message = "The RPC server is unavailable." Then

                                WriteLog(logFolder, "RPC error, restarting MS Word...")
                                NITF32_Converter.Close("")
                                NITF32_Converter = New WordMl2Nitf(AppSettings("xsltFile"))
                            End If
                        End Try
                    End If

                    'Post processing if conversion was ok
                    If ok Then
                        WriteLog(logFolder, "Postprocessing " & m.Subject & "...")

                        'Write the XML in the correct Encoding and code the special chars 
                        xml = Replace(xml, "encoding=""UTF-16""", "encoding=""iso-8859-1""", 1, 1)

                        'Fix tabtables
                        xml = Replace(xml, "-start-table-", vbCrLf & "<table>" & vbCrLf)
                        xml = Replace(xml, "-end-table-", vbCrLf & "</table>" & vbCrLf)

                        'Do some cleanup of journalists use of "<<- and so on

                        'Fix " -> � �
                        Dim pattern As String = "([-\s\(>" & Chr(150) & "])[""" & Chr(147) & Chr(148) & Chr(132) & "]+([^<]+?)[""" & Chr(147) & Chr(148) & Chr(132) & "]+([^>])"
                        xml = RegularExpressions.Regex.Replace(xml, pattern, "$1" & Chr(171) & "$2" & Chr(187) & "$3")

                        'Fix tankestrek - alltid mellomrom bak
                        pattern = "([\s>])[-" & Chr(150) & "] ?([A-Za-z������" & Chr(171) & "])"
                        xml = RegularExpressions.Regex.Replace(xml, pattern, "$1" & Chr(150) & " $2")

                        'Encode special chars
                        pattern = "([" & Chr(159) & "-" & Chr(128) & "])"
                        Dim rx As RegularExpressions.Regex = New RegularExpressions.Regex(pattern, RegularExpressions.RegexOptions.Compiled Or RegularExpressions.RegexOptions.IgnoreCase Or RegularExpressions.RegexOptions.Multiline)

                        Dim mtc As RegularExpressions.Match
                        For Each mtc In rx.Matches(xml)
                            xml = xml.Replace(mtc.Groups(1).Value, "&#" & AscW(mtc.Groups(1).Value) & ";")
                        Next

                        'Dump to file
                        WriteFile(xmlOut, xml, False, "iso-8859-1")
                        WriteLog(logFolder, "Message saved to: " & Path.GetFileName(xmlOut))

                        'Klippe direktesaker her??
                        If clipDirekte Then

                            Dim folder, sendDir, sendDP As String
                            Dim strXMLDirekte As String
                            Dim xmlDirDoc As XmlDocument = New XmlDocument

                            Try
                                xmlDirDoc.LoadXml(xml)
                                folder = xmlDirDoc.SelectSingleNode("/nitf/head/meta[@name='NTBTjeneste']/@content").InnerText
                                sendDir = xmlDirDoc.SelectSingleNode("/nitf/head/meta[@name='NTBSendTilDirekte']/@content").InnerText
                                sendDP = xmlDirDoc.SelectSingleNode("/nitf/head/meta[@name='NTBSendTilDirektePluss']/@content").InnerText
                            Catch
                            End Try

                            'Jepp, tror det...
                            If folder = "Ut-Satellitt" And (sendDir = "True" Or sendDP = "True") Then
                                Dim clipper As ClipDirekteFeed = New ClipDirekteFeed(xsltDirekteClipper)

                                ' Make NITF-XML-file
                                xmlOut = xmlOut.Replace("Nyhet", "Direkte")

                                'Transform
                                strXMLDirekte = clipper.ClipNITFDocument(xml, xmlOut)
                                xmlDirDoc.LoadXml(strXMLDirekte)

                                'Encode special chars
                                For Each mtc In rx.Matches(strXMLDirekte)
                                    strXMLDirekte = strXMLDirekte.Replace(mtc.Groups(1).Value, "&#" & AscW(mtc.Groups(1).Value) & ";")
                                Next

                                'Write the XML in the correct Encoding
                                strXMLDirekte = Replace(strXMLDirekte, "encoding=""UTF-16""", "encoding=""iso-8859-1""", 1, 1)

                                WriteFile(xmlOut, strXMLDirekte, False, "iso-8859-1")
                                WriteLog(logFolder, "Message saved to: " & Path.GetFileName(xmlOut))
                            End If

                        End If

                        'Klippe NPK direktesaker her??
                        If clipNPKDirekte Then
                            Dim folder As String
                            Dim strXMLDirekte As String
                            Dim xmlDirDoc As XmlDocument = New XmlDocument

                            Try
                                xmlDirDoc.LoadXml(xml)
                                folder = xmlDirDoc.SelectSingleNode("/nitf/head/meta[@name='foldername']/@content").InnerText
                            Catch
                            End Try

                            'Jepp, tror det...
                            If folder = "Ut-NPKDirekte" Then
                                Dim clipper As ClipTikkerFeed = New ClipTikkerFeed(xsltNPKDirekteClipper)

                                ' Make NITF-XML-file
                                xmlOut = xmlOut.Replace("Nyhet", "NPKDirekte")

                                'Transform
                                strXMLDirekte = clipper.ClipNITFDocument(xml, xmlOut)
                                xmlDirDoc.LoadXml(strXMLDirekte)

                                'Encode special chars
                                For Each mtc In rx.Matches(strXMLDirekte)
                                    strXMLDirekte = strXMLDirekte.Replace(mtc.Groups(1).Value, "&#" & AscW(mtc.Groups(1).Value) & ";")
                                Next

                                'Write the XML in the correct Encoding
                                strXMLDirekte = Replace(strXMLDirekte, "encoding=""UTF-16""", "encoding=""iso-8859-1""", 1, 1)

                                WriteFile(xmlOut, strXMLDirekte, False, "iso-8859-1")
                                WriteLog(logFolder, "Message saved to: " & Path.GetFileName(xmlOut))
                            End If
                        End If

                        'Klippe tikker her??
                        If clipTikker Then
                            Dim folder As String
                            Dim strXMLDirekte As String
                            Dim xmlDirDoc As XmlDocument = New XmlDocument

                            Try
                                xmlDirDoc.LoadXml(xml)
                                folder = xmlDirDoc.SelectSingleNode("/nitf/head/meta[@name='foldername']/@content").InnerText
                            Catch
                            End Try

                            'Jepp, tror det...
                            If folder = "Ut-Tikker" Then
                                Dim clipper As ClipTikkerFeed = New ClipTikkerFeed(xsltTikkerClipper)

                                ' Make NITF-XML-file
                                xmlOut = xmlOut.Replace("Nyhet", "Tikker")

                                'Transform
                                strXMLDirekte = clipper.ClipNITFDocument(xml, xmlOut)
                                xmlDirDoc.LoadXml(strXMLDirekte)

                                'Encode special chars
                                For Each mtc In rx.Matches(strXMLDirekte)
                                    strXMLDirekte = strXMLDirekte.Replace(mtc.Groups(1).Value, "&#" & AscW(mtc.Groups(1).Value) & ";")
                                Next

                                'Write the XML in the correct Encoding
                                strXMLDirekte = Replace(strXMLDirekte, "encoding=""UTF-16""", "encoding=""iso-8859-1""", 1, 1)

                                WriteFile(xmlOut, strXMLDirekte, False, "iso-8859-1")
                                WriteLog(logFolder, "Message saved to: " & Path.GetFileName(xmlOut))
                            End If
                        End If

                        Try
                            Dim tmp As MAPI.Message = m.MoveTo(GetFolderByName(node.Attributes("done").Value).ID)
                            tmp.Update(True, True)
                            WriteLog(logFolder, "Message export complete. Message moved to '" & node.Attributes("done").Value & "'")
                        Catch ex As Exception
                            i += 1
                            WriteErr(logFolder, "Failed to move message to '" & node.Attributes("done").Value & "'", ex)
                        End Try
                    Else

                        Try
                            Dim tmp As MAPI.Message = m.MoveTo(GetFolderByName(node.Attributes("error").Value).ID)
                            tmp.Update(True, True)
                            WriteLog(logFolder, "Message export failed. Message moved to '" & node.Attributes("error").Value & "'")
                        Catch ex As Exception
                            i += 1
                            WriteErr(logFolder, "Failed to move message to '" & node.Attributes("error").Value & "'", ex)
                        End Try

                    End If

                    'Cleanup
                    If removeTemp Then
                        File.Delete(rtfTmp)
                        File.Delete(wmlTmp)
                        File.Delete(xmlTmp)
                    End If

                End While

            Next

        Catch ex As Exception
            WriteErr(logFolder, "General system error in MessagePollTimer_Elapsed. (File: " & xmlOut & " )", ex)
        End Try

        Try
            'Restart timer
            _ready.Set()
            MessagePollTimer.Interval = AppSettings("pollInterval") * 1000
            MessagePollTimer.Start()
            WriteLog(logFolder, "Polling restarted, " & AppSettings("pollInterval") & " seconds interval.")
        Catch ex As Exception
            WriteErr(logFolder, "Timer / Sync-object error.", ex)
        End Try

    End Sub

    'Loops the field map and fetches field content, returned as xml
    Public Function GetMessageHeader(ByVal msg As MAPI.Message, ByVal fld As MAPI.Folder) As String

        Dim headerList As Hashtable = New Hashtable
        Dim header As StringBuilder = New StringBuilder("<head>" & vbCrLf)
        Dim fields As MAPI.Fields = msg.Fields

        Dim dtNow As Date = Now

        'Load MAPI.Fields into header hash
        Try
            Dim f As MAPI.Field
            For Each f In msg.Fields
                headerList(f.Name) = f.Value
            Next
        Catch ex As Exception
            WriteErr(logFolder, "Message's MAPI.Fields loop failed", ex)
            Return ""
        End Try

        header.Append(CreateMetaNode("timestamp", Format(dtNow, "yyyy.MM.dd HH:mm:ss")))
        header.Append(CreateMetaNode("print-date", Format(dtNow, "dd.MM.yyyy HH:mm")))
        header.Append(CreateMetaNode("nitf-date", Format(dtNow, "yyyyMMddTHHmmss")))
        header.Append(CreateMetaNode("pub-date", Format(dtNow, "yyyy-MM-ddTHH:mm:ss")))
        header.Append(CreateMetaNode("NTBIPTCSequence", Format(Get_Seq(), "0000")))

        'Dim foundFolderHeader As Boolean
        'foundFolderHeader = False

        Dim id As String
        Dim node As XmlNode
        Dim sg As String
        Dim ug As String
        For Each node In fieldMap
            Try

                Select Case node.Attributes("notabene").Value
                    Case "subject"
                        header.Append(CreateMetaNode(node.Attributes("xml").Value, msg.Subject))
                    Case "foldername"
                        'foundFolderHeader = True
                        header.Append(CreateMetaNode(node.Attributes("notabene").Value, fld.Name))
                        sg = headerList("NTBStoffgruppe")
                        If sg = "Formidlingstjenester" Then
                            header.Append(CreateMetaNode(node.Attributes("xml").Value, "Formidlingstjenester"))
                        ElseIf sg = "Dagen i dag" Or _
                               sg = "Jubilant-omtaler" Or _
                               sg = "Kultur og underholdning" Or _
                               sg = "Showbiz" Or _
                               sg = "Feature" Or _
                               sg = "Moter/Trender" Or _
                               sg = "Helse" Or _
                               sg = "V5/DD" Or _
                               sg = "V75" Or _
                               sg = "Langodds" Or _
                               sg = "Tippevurdering" Or _
                               sg = "Tippefakta" Or _
                               sg = "Reiser" Then
                            header.Append(CreateMetaNode(node.Attributes("xml").Value, "Spesialtjenester"))
                        ElseIf fld.Name = "Ut-Direkte" Then
                            header.Append(CreateMetaNode(node.Attributes("xml").Value, "Direktetjenesten"))
                        ElseIf fld.Name = "Ut-Satellitt" Then
                            header.Append(CreateMetaNode(node.Attributes("xml").Value, "Nyhetstjenesten"))
                        ElseIf fld.Name = "Ut-Internt" Or fld.Name = "Ut-Portal" Then
                            header.Append(CreateMetaNode(node.Attributes("xml").Value, "Interntjenesten"))
                        ElseIf fld.Name = "Ut-Repetisjoner" Then
                            header.Append(CreateMetaNode(node.Attributes("xml").Value, "Repetisjonstjenesten"))
                        ElseIf fld.Name = "Ut-Lettlest" Then
                            header.Append(CreateMetaNode(node.Attributes("xml").Value, "Lettlesttjenesten"))
                        ElseIf fld.Name = "Ut-Tikker" Then
                            header.Append(CreateMetaNode(node.Attributes("xml").Value, "Tikkertjenesten"))
                        ElseIf fld.Name = "Ut-NPK" Then
                            header.Append(CreateMetaNode(node.Attributes("xml").Value, "NPKTjenesten"))
                        ElseIf fld.Name = "Ut-NPKDirekte" Then
                            header.Append(CreateMetaNode(node.Attributes("xml").Value, "NPKDirektetjenesten"))
                        ElseIf fld.Name = "Ut-Videotjeneste" Then
                            header.Append(CreateMetaNode(node.Attributes("xml").Value, "Videotjenesten"))
                        ElseIf fld.Name = "Ut-NyheterFraNorge" Then
                            header.Append(CreateMetaNode(node.Attributes("xml").Value, "NyheterFraNorge"))
                        ElseIf fld.Name = "Ut-Tema" Then
                            header.Append(CreateMetaNode(node.Attributes("xml").Value, "NTBTema"))

                        ElseIf fld.Name = "lch" Then ' DEBUG!
                            header.Append(CreateMetaNode(node.Attributes("xml").Value, "NTBTema"))

                        Else
                            header.Append(CreateMetaNode(node.Attributes("xml").Value, "Spesialtjenester"))
                        End If
                    Case "NTBID"
                        id = headerList(node.Attributes("notabene").Value)
                        header.Append(CreateMetaNode(node.Attributes("xml").Value, id))
                        header.Append(CreateMetaNode("NTBID", Regex.Replace(id, "_\d+$", "")))
                    Case "NTBStoffgruppe"
                        ug = headerList("NTBUndergruppe")
                        sg = headerList(node.Attributes("notabene").Value)
                        'Map stoffgruppe
                        If ug = "PRM-tjenesten" Then
                            header.Append(CreateMetaNode(node.Attributes("xml").Value, "PRM-NTB"))
                            header.Append(CreateMetaNode("NTBUndergruppe", "Fulltekstmeldinger"))
                        ElseIf fld.Name = "Ut-KulturDirekte" Then
                            header.Append(CreateMetaNode(node.Attributes("xml").Value, "Showbiz"))
                        Else
                            header.Append(CreateMetaNode(node.Attributes("xml").Value, headerList(node.Attributes("notabene").Value)))
                        End If
                    Case Else
                        ug = headerList("NTBUndergruppe")
                        If Not (node.Attributes("notabene").Value = "NTBUndergruppe" And (ug = "PRM-tjenesten" Or fld.Name = "Ut-KulturDirekte")) Then
                            header.Append(CreateMetaNode(node.Attributes("xml").Value, headerList(node.Attributes("notabene").Value)))
                        End If
                End Select

            Catch ex As Exception
                WriteErr(logFolder, "Failed to get header from message: " & node.Attributes("notabene").Value & " ( ID: " & id & " )", ex)
                Return ""
            End Try
        Next

        'If Not foundFolderHeader Then
        'header.Append(CreateMetaNode("foldername", fld.Name))
        'End If

        header.Append("</head>")
        Return header.ToString
    End Function

    'Creates a meta header node
    Private Function CreateMetaNode(ByVal strName As String, ByVal strContent As String) As String
        If Not strContent Is Nothing Then
            strContent = strContent.Replace("&", "&amp;")
            strContent = strContent.Replace("'", "&apos;")
            strContent = strContent.Replace("<", "&lt;")
            strContent = strContent.Replace(">", "&gt;")
            strContent = RegularExpressions.Regex.Replace(strContent, "[""" & Chr(147) & Chr(148) & Chr(132) & "]", "&quot;")
        End If

        Return "<meta name=""" & strName & """ content=""" & strContent & """/>" & vbCrLf
    End Function

    'Makes a filename from the header
    Private Function MakeFileNameFromHead(ByRef xmlHd As String) As String
        Dim xmlHead As XmlDocument = New XmlDocument
        Dim sbFileName As New System.Text.StringBuilder

        xmlHead.LoadXml(xmlHd)

        On Error Resume Next
        sbFileName.Append(xmlHead.SelectSingleNode("//head/meta[@name='timestamp']/@content").InnerText.Replace(" ", "_"))
        sbFileName.Append("_")

        Dim tmp As String = xmlHead.SelectSingleNode("//head/meta[@name='NTBTjeneste']/@content").InnerText
        tmp = tmp.Substring(0, tmp.IndexOf("tjeneste"))
        sbFileName.Append(tmp.TrimEnd("s"))
        sbFileName.Append("_")

        'Standard header
        sbFileName.Append(xmlHead.SelectSingleNode("//head/meta[@name='NTBStoffgruppe']/@content").InnerText.Substring(0, 3).ToUpper)

        'Direkte clipping
        sbFileName.Append(xmlHead.SelectSingleNode("//head/tobject/@tobject.type").InnerText.Substring(0, 3).ToUpper)
        sbFileName.Append("_")

        'Standard header
        sbFileName.Append(xmlHead.SelectSingleNode("//head/meta[@name='NTBUndergruppe']/@content").InnerText.Substring(0, 3).ToUpper)

        'Direkte clipping
        sbFileName.Append(xmlHead.SelectSingleNode("//head/tobject/tobject.property/@tobject.property.type").InnerText.Substring(0, 3).ToUpper)
        sbFileName.Append("_")

        sbFileName.Append(xmlHead.SelectSingleNode("//head/meta[@name='subject']/@content").InnerText.ToLower)

        sbFileName.Replace(" ", "_")
        sbFileName.Replace(":", "-")
        sbFileName.Replace(".", "-")
        sbFileName.Replace("/", "-")
        sbFileName.Replace("\", "-")
        sbFileName.Replace("&", "-")

        Dim strTemp As String = sbFileName.ToString

        strTemp = System.Text.RegularExpressions.Regex.Replace(strTemp, "[^0-9A-Za-z\ _\-�����]", "")

        sbFileName = New System.Text.StringBuilder(strTemp)

        sbFileName.Replace("�", "a")
        sbFileName.Replace("�", "o")
        sbFileName.Replace("�", "a")
        sbFileName.Replace("�", "a")
        sbFileName.Replace("�", "e")
        sbFileName.Replace("hast-", "HAST-")

        xmlHead.SelectSingleNode("//head/meta[@name='filename']/@content").InnerText = sbFileName.ToString & ".xml"
        xmlHd = xmlHead.InnerXml

        Return sbFileName.ToString
    End Function

End Class
