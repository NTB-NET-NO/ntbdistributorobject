Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Xml.Xsl
Imports System.Xml
Imports System.IO
Imports System.Configuration.ConfigurationSettings

Public Class WordMl2Nitf
    'Implements System.ComponentModel.ISynchronizeInvoke
    'Public Overridable Overloads Function BeginInvoke(ByVal method As System.Delegate, ByVal args() As Object) As System.IAsyncResult
    'End Function

    Private objWord As Word.ApplicationClass
    Private objWordDoc As Word.DocumentClass

    Private xsltWordML2Nitf As XslTransform
    Private xsltHead As XslTransform

    Private xslt4WordML2Nitf As MSXML2.IXSLProcessor  ' XSLT Problem workaround

    Public IPTC_sequence As Integer = 1
    Public strXmlMapFile As String
    Public writeDebugXml As Boolean
    Public deleteWordMlTempFile As Boolean

    Public Sub New(ByVal strWord2NITFXsltFile As String, ByVal strXsltHead As String, ByVal seqFile As String)
        objWord = New Word.ApplicationClass
        LoadXsltFiles(strWord2NITFXsltFile, strXsltHead)
        'LoadSequence(seqFile, True)
    End Sub

    Public Sub New(ByVal strWord2NITFXsltFile As String)
        objWord = New Word.ApplicationClass
        LoadXsltFiles(strWord2NITFXsltFile)
    End Sub

    Public Sub Close(ByVal seqFile As String)
        'LoadSequence(seqFile, False)
        objWord.Quit()
        objWordDoc = Nothing
        objWord = Nothing
    End Sub

    Private Sub LoadSequence(ByVal file As String, ByVal load As Boolean)
        Try
            If load Then
                IPTC_sequence = HelperFunctions.ReadFile(file).Trim()
            Else
                HelperFunctions.WriteFile(file, IPTC_sequence, False)
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub Inc_Seq()
        IPTC_sequence += 1
        If IPTC_sequence > 5999 Then IPTC_sequence = 1
    End Sub

    Private Sub LoadXsltFiles(ByVal xsltFile As String)
        xsltWordML2Nitf = New XslTransform
        xsltWordML2Nitf.Load(xsltFile)

        ' XSLT Problem workaround, using MSXML4.DLL instead of .NET-xml components :
        'Workaround creates cleaner xml-nitf

        Dim xsltDoc As MSXML2.FreeThreadedDOMDocument
        Dim xmlError As MSXML2.IXMLDOMParseError
        Dim xslt As MSXML2.XSLTemplate

        xslt = New MSXML2.XSLTemplate
        xsltDoc = New MSXML2.FreeThreadedDOMDocument

        xsltDoc.load(xsltFile)
        xmlError = xsltDoc.parseError

        If (xmlError.errorCode <> 0) Then
            'MsgBox(xmlError.reason & " i linje: " & xmlError.line)
        End If

        xslt.stylesheet = xsltDoc
        xslt4WordML2Nitf = xslt.createProcessor

        ' End XSLT Problem workaround:        

    End Sub


    Private Sub LoadXsltFiles(ByVal xsltFile As String, ByVal strXsltHead As String)
        xsltWordML2Nitf = New XslTransform
        xsltWordML2Nitf.Load(xsltFile)

        xsltHead = New XslTransform
        xsltHead.Load(strXsltHead)

        ' XSLT Problem workaround, using MSXML4.DLL instead of .NET-xml components :
        'Workaround creates cleaner xml-nitf

        Dim xsltDoc As MSXML2.FreeThreadedDOMDocument
        Dim xmlError As MSXML2.IXMLDOMParseError
        Dim xslt As MSXML2.XSLTemplate

        xslt = New MSXML2.XSLTemplate
        xsltDoc = New MSXML2.FreeThreadedDOMDocument

        xsltDoc.load(xsltFile)
        xmlError = xsltDoc.parseError

        If (xmlError.errorCode <> 0) Then
            'MsgBox(xmlError.reason & " i linje: " & xmlError.line)
        End If

        xslt.stylesheet = xsltDoc
        xslt4WordML2Nitf = xslt.createProcessor

        ' End XSLT Problem workaround:        

    End Sub

    Public Function ConvertDoc(ByVal strFileName As String, Optional ByVal strXmlHead As String = "", Optional ByVal strTempFile As String = "", Optional ByVal strDebugXML As String = "") As String

        If strTempFile = "" Then
            strTempFile = Path.GetDirectoryName(strFileName) & Path.DirectorySeparatorChar & Path.GetFileNameWithoutExtension(strFileName) & ".xml"
        End If

        'Lagrer som XML og lukker
        objWordDoc = objWord.Documents.Open(strFileName)
        objWordDoc.SaveAs(strTempFile, Word.WdSaveFormat.wdFormatXML)
        objWordDoc.Close()

        objWordDoc = Nothing

        Dim strWordML As String = HelperFunctions.ReadFile(strTempFile, "utf-8")

        strWordML = strWordML.Replace("<?mso-application progid=""Word.Document""?>", "")
        strWordML = strWordML.Replace("</o:DocumentProperties>", "</o:DocumentProperties>" & strXmlHead)

        ' Write to file for debug and input for development of XSLT-stylesheet only:
        If strDebugXML <> "" Then
            Dim strTempXml As String = strWordML
            strTempXml = strTempXml.Replace("<!--", vbCrLf & "<!--")
            strTempXml = strTempXml.Replace("<meta", vbCrLf & "<meta")
            strTempXml = strTempXml.Replace("<nitf", vbCrLf & "<nitf")
            strTempXml = strTempXml.Replace("<o:", vbCrLf & "<o:")
            strTempXml = strTempXml.Replace("<w:", vbCrLf & "<w:")
            HelperFunctions.WriteFile(strDebugXML, strTempXml, , "utf-8")
        End If

        Dim strNITFBody As String = MakeXsltTransform4(strWordML)

        If deleteWordMlTempFile Then
            File.Delete(strTempFile)
        End If

        Return strNITFBody
    End Function

    Protected Function MakeXsltTransform4(ByRef strXml As String) As String

        '.NET XslTransform Problem workaround:        
        'Har problemer med formattering .NET XslTransform, og bruker her MSXML4.dll i stedet!

        Dim xml4Doc As MSXML2.DOMDocument = New MSXML2.DOMDocument
        Dim strXml4 As String ' = xslt4WordML2Nitf.output

        xml4Doc.loadXML(strXml)
        xslt4WordML2Nitf.input = xml4Doc

        'xslt4WordML2Nitf.

        Dim test As Boolean = xslt4WordML2Nitf.transform()
        strXml4 = xslt4WordML2Nitf.output

        strXml4 = BulkReplace(strXml4, "<nitf ", ">", "<nitf version=""-//IPTC//DTD NITF 3.2//EN"" change.date=""October 10, 2003"" change.time=""19:30"" baselang=""no-NO"">", 0, 1)
        Return strXml4

        'Dim xwNitf As XmlTextWriter = New XmlTextWriter(strNitfFile, System.Text.Encoding.GetEncoding("iso-8859-1"))
        'Dim xmlDoc As XmlDocument = New XmlDocument
        'xmlDoc.LoadXml(strXml)

        'xwNitf.Formatting = Formatting.Indented
        'xwNitf.IndentChar = vbTab
        'xwNitf.Indentation = 1

        'xwNitf.WriteRaw(Replace(strXml4, "encoding=""UTF-16""", "encoding=""iso-8859-1""", 1, 1))
        'xwNitf.WriteRaw(Replace(strXml4, "encoding=""UTF-16""", "encoding=""iso-8859-1""", 1, 1))

        'xwNitf.Flush()
        'xwNitf.Close()

    End Function

    Protected Function MakeXsltTransform(ByVal xsltProc As Xsl.XslTransform, ByRef strXml As String) As String

        Dim ioXml As IO.MemoryStream = New IO.MemoryStream
        Dim srXml As IO.StreamReader = New StreamReader(ioXml, System.Text.Encoding.GetEncoding("UTF-8")) 'iso-8859-1"))
        Dim xw As XmlTextWriter = New XmlTextWriter(ioXml, System.Text.Encoding.GetEncoding("UTF-8")) 'iso-8859-1"))

        Dim xmlDoc As XmlDocument = New XmlDocument

        'xw.WriteStartDocument(False)
        xw.Formatting = Formatting.None 'Formatting.Indented
        xw.Indentation = 0
        'xw.IndentChar = vbTab

        xmlDoc.LoadXml(strXml)
        'xsltProc.Transform(xmlDoc, Nothing, ioXml, Nothing)
        Dim xmlRes As XmlResolver

        xsltProc.Transform(xmlDoc, Nothing, xw, Nothing)
        ioXml.Position = 0
        Return srXml.ReadToEnd

    End Function

    Public Function LoadNotabeneFieldsHeader(ByVal strMapFile As String, ByRef ObjMessage As Object, ByRef strError As String, ByVal strDirectory As String)
        Dim strFieldName As String
        Dim strXmlFieldName As String
        Dim strXmlFieldContent As String
        Dim strXmlHead As String

        Dim xmlMap As XmlDocument = New XmlDocument
        xmlMap.Load(strMapFile)

        strError = ""
        strXmlHead = "<head>" & vbCrLf

        Dim dtNow As Date = Now

        AddXmlHeadNode(strXmlHead, "timestamp", Format(dtNow, "yyyy.MM.dd HH:mm:ss"))
        AddXmlHeadNode(strXmlHead, "NitfDate", Format(dtNow, "yyyyMMddTHHmmss"))
        AddXmlHeadNode(strXmlHead, "ntb-date", Format(dtNow, "dd.MM.yyyy HH:mm:ss"))
        AddXmlHeadNode(strXmlHead, "foldername", strDirectory)

        Dim mapNode As XmlNode
        For Each mapNode In xmlMap.SelectNodes("/notabene/field")
            Try
                strFieldName = mapNode.Attributes("notabene").InnerText
                strXmlFieldName = mapNode.Attributes("xml").InnerText

                ' Lars! Her kan du legge til en funksjon GetNotabeneFieldVal som lager henter feltverdier fra Notabene
                'strXmlFieldContent = GetNotabeneFieldVal(strFieldName)
                strXmlFieldContent = ObjMessage.Fields(strFieldName).Value

                ' Kode som legger til xml 
                AddXmlHeadNode(strXmlHead, strXmlFieldName, strXmlFieldContent)
            Catch e As Exception
                strError = e.Message & vbCrLf & e.StackTrace
            End Try
        Next

        strXmlHead &= "</head>" & vbCrLf
        Return strXmlHead
    End Function

    Public Function LoadNotabeneFieldsHeaderFromRTF(ByVal strMapFile As String, ByVal headerFip As String, ByRef strError As String, ByVal strDirectory As String)
        Dim strFieldName As String
        Dim strFipName As String
        Dim strXmlFieldName As String
        Dim strXmlFieldContent As String
        Dim strXmlHead As String

        Dim xmlMap As XmlDocument = New XmlDocument
        xmlMap.Load(strMapFile)

        strError = ""
        strXmlHead = "<head>" & vbCrLf

        Dim dtNow As Date = Now

        AddXmlHeadNode(strXmlHead, "timestamp", Format(dtNow, "yyyy.MM.dd HH:mm:ss"))
        AddXmlHeadNode(strXmlHead, "NitfDate", Format(dtNow, "yyyyMMddTHHmmss"))
        AddXmlHeadNode(strXmlHead, "ntb-date", Format(dtNow, "dd.MM.yyyy HH:mm:ss"))
        AddXmlHeadNode(strXmlHead, "foldername", strDirectory)

        Dim mapNode As XmlNode
        For Each mapNode In xmlMap.SelectNodes("/notabene/field")
            Try
                strXmlFieldContent = ""
                strFipName = mapNode.Attributes("fip").InnerText
                strFieldName = mapNode.Attributes("notabene").InnerText
                strXmlFieldName = mapNode.Attributes("xml").InnerText

                ' Lars! Her kan du legge til en funksjon GetNotabeneFieldVal som lager henter feltverdier fra Notabene
                'strXmlFieldContent = GetNotabeneFieldVal(strFieldName)
                'strXmlFieldContent = ObjMessage.Fields(strFieldName).Value

                'Eller en funksjon som sl�r opp i headeren og finner verdi basert p� FIP kode... :)
                strXmlFieldContent = RegularExpressions.Regex.Match(headerFip, strFipName & ":([^\r\n]*)").Groups(1).Value

                'Fikser opp attributtverdier

                'Fix " -> � �
                Dim pattern As String = "[""" & Chr(147) & Chr(148) & Chr(132) & "]+(.+?)[""" & Chr(147) & Chr(148) & Chr(132) & "]+"
                strXmlFieldContent = Regex.Replace(strXmlFieldContent, pattern, Chr(171) & "$1" & Chr(187))

                ' Kode som legger til xml 
                If strXmlFieldContent <> "" Then AddXmlHeadNode(strXmlHead, strXmlFieldName, strXmlFieldContent)
            Catch e As Exception
                strError = e.Message & vbCrLf & e.StackTrace
            End Try
        Next

        strXmlHead &= "</head>" & vbCrLf
        Return strXmlHead
    End Function


    Protected Sub AddXmlHeadNode(ByRef strXmlHead As String, ByVal strName As String, ByVal strContent As String)
        strContent = strContent.Replace("&", "&amp;")
        strContent = strContent.Replace("'", "&apos;")
        strContent = strContent.Replace("<", "&lt;")
        strContent = strContent.Replace(">", "&gt;")
        strContent = Regex.Replace(strContent, "[""" & Chr(147) & Chr(148) & Chr(132) & "]", "&quot;")

        strXmlHead &= "<meta name=""" & strName & """ content=""" & strContent & """/>" & vbCrLf
    End Sub


    Public Function getXmlHeaderAndRtf(ByVal strXmlInputFile, ByRef strRtfFile) As String
        Dim strXml As String
        Dim strRtf As String

        Dim ext As String
        Dim err As String
        Dim strXmlHeader As String

        strXml = HelperFunctions.ReadFile(strXmlInputFile)
        ext = Path.GetExtension(strXmlInputFile).ToLower()

        'Branch, to hand raw RTF and XML
        If (ext = ".xml") Then

            'Fetch data from input file
            strRtf = GetStringPortion(strXml, "<rtftext><![CDATA[", "]]></rtftext>")
            strXmlHeader = GetStringPortion(strXml, "<message>", "<rtftext>")

            'Check for errors
            Dim ex As Exception = New Exception("Data missing or wrong format ( DistributorXML: " & strXmlInputFile & " )")
            If strRtf = "" Or strXmlHeader = "" Then Throw ex

            'Process and return
            HelperFunctions.WriteFile(strRtfFile, strRtf, False)
            strXmlHeader = "<message>" & strXmlHeader & "</message>"

            strXmlHeader = MakeXsltTransform(xsltHead, strXmlHeader)
        ElseIf (ext = ".rtf") Then
            Try
                strXmlHeader = strXml.Split("~")(1)
                strRtf = strXml.Split("~", 3)(2) 'strXml.Substring(strXml.LastIndexOf("~" & vbCrLf) + 3)
                strRtf = strRtf.TrimStart()
            Catch
            End Try

            'Check for errors
            Dim ex As Exception = New Exception("Data missing or wrong format ( DistributorRTF: " & strXmlInputFile & " )")
            If strRtf = "" Or strXmlHeader = "" Then Throw ex

            'Dump RTF for word conversion
            HelperFunctions.WriteFile(strRtfFile, strRtf, False)

            'Do header
            strXmlHeader = LoadNotabeneFieldsHeaderFromRTF(strXmlMapFile, strXmlHeader, err, Path.GetDirectoryName(strXmlInputFile))

        Else
            Dim ex As Exception = New Exception("Data missing or wrong format ( Unknown input file: " & strXmlInputFile & " )")
            Throw ex
        End If

        'Fix header
        FixHeader(strXmlHeader)

        Return strXmlHeader

    End Function

    'Fix up some of the fields, sound/short NTB id
    Private Sub FixHeader(ByRef strHeader As String)
        Dim doc As XmlDocument = New XmlDocument
        doc.LoadXml(strHeader)

        'Lydfilnavn uten ext.
        Dim lydfil As String
        Try
            lydfil = Path.GetFileNameWithoutExtension(doc.SelectSingleNode("/head/meta[@name='ntb-lydfil']/@content").InnerText)
        Catch e As Exception
            'No sound
        End Try

        If lydfil <> "" Then
            strHeader = Replace(strHeader, "</head>", "<meta name=""ntb-lyd"" content=""" & lydfil & """/></head>")
        End If

        'Multimediefil uten ext.
        Dim multimediefil As String
        Try
            multimediefil = Path.GetFileNameWithoutExtension(doc.SelectSingleNode("/head/meta[@name='ntb-multimediefil']/@content").InnerText)
        Catch e As Exception
            'No multimedia
        End Try

        If multimediefil <> "" Then
            strHeader = Replace(strHeader, "</head>", "<meta name=""ntb-multimediefil"" content=""" & multimediefil & """/></head>")
        End If

        'Short NTBID
        Dim id As String
        Try
            id = doc.SelectSingleNode("/head/meta[@name='NTBID']/@content").InnerText
        Catch e As Exception
            'No ID??
        End Try

        If id <> "" Then
            strHeader = Replace(strHeader, "</head>", "<meta name=""ntb-idstr"" content=""" & id & """/></head>")
            strHeader = Replace(strHeader, id, Regex.Replace(id, "_\d+$", ""), , 1)
        End If

        'IPTC-generation sequence number
        strHeader = Replace(strHeader, "<meta name=""NTBID""", "<meta name=""NTBIPTCSequence"" content=""" & Format(IPTC_sequence, "0000") & """/>" & vbCrLf & "<meta name=""NTBID""")
        Inc_Seq()

        'NTBTjeneste
        Dim sg As String
        Dim ug As String
        Dim mappe As String
        Try
            sg = doc.SelectSingleNode("/head/meta[@name='NTBStoffgruppe']/@content").InnerText
            ug = doc.SelectSingleNode("/head/meta[@name='NTBUndergruppe']/@content").InnerText
            mappe = doc.SelectSingleNode("/head/meta[@name='foldername']/@content").InnerText
        Catch e As Exception
            'No folder or Stoffgruppe
        End Try

        'Add tjeneste field
        If sg = "Formidlingstjenester" Then
            strHeader = Replace(strHeader, "<meta name=""NTBMeldingsSign""", "<meta name=""NTBTjeneste"" content=""Formidlingstjenester""/>" & vbCrLf & "<meta name=""NTBMeldingsSign""")
        ElseIf sg = "Dagen i dag" Or _
               sg = "Jubilant-omtaler" Or _
               sg = "Kultur og underholdning" Or _
               sg = "Showbiz" Or _
               sg = "Feature" Or _
               sg = "Moter/Trender" Or _
               sg = "Helse" Or _
               sg = "V5/DD" Or _
               sg = "V75" Or _
               sg = "Langodds" Or _
               sg = "Tippevurdering" Or _
               sg = "Tippefakta" Or _
               sg = "Reiser" Then
            strHeader = Replace(strHeader, "<meta name=""NTBMeldingsSign""", "<meta name=""NTBTjeneste"" content=""Spesialtjenester""/>" & vbCrLf & "<meta name=""NTBMeldingsSign""")
        ElseIf mappe = "Ut-Direkte" Then
            strHeader = Replace(strHeader, "<meta name=""NTBMeldingsSign""", "<meta name=""NTBTjeneste"" content=""Direktetjenesten""/>" & vbCrLf & "<meta name=""NTBMeldingsSign""")
        ElseIf mappe = "Ut-Satellitt" Then
            strHeader = Replace(strHeader, "<meta name=""NTBMeldingsSign""", "<meta name=""NTBTjeneste"" content=""Nyhetstjenesten""/>" & vbCrLf & "<meta name=""NTBMeldingsSign""")
        ElseIf mappe = "Ut-Internt" Or mappe = "Ut-Portal" Then
            strHeader = Replace(strHeader, "<meta name=""NTBMeldingsSign""", "<meta name=""NTBTjeneste"" content=""Interntjenesten""/>" & vbCrLf & "<meta name=""NTBMeldingsSign""")
        ElseIf mappe = "Ut-Repetisjoner" Then
            strHeader = Replace(strHeader, "<meta name=""NTBMeldingsSign""", "<meta name=""NTBTjeneste"" content=""Repetisjonstjenesten""/>" & vbCrLf & "<meta name=""NTBMeldingsSign""")
        ElseIf mappe = "Ut-Lettlest" Then
            strHeader = Replace(strHeader, "<meta name=""NTBMeldingsSign""", "<meta name=""NTBTjeneste"" content=""Lettlesttjenesten""/>" & vbCrLf & "<meta name=""NTBMeldingsSign""")
        ElseIf mappe = "Ut-Tikker" Then
            strHeader = Replace(strHeader, "<meta name=""NTBMeldingsSign""", "<meta name=""NTBTjeneste"" content=""Tikkertjenesten""/>" & vbCrLf & "<meta name=""NTBMeldingsSign""")
        ElseIf mappe = "Ut-NPK" Then
            strHeader = Replace(strHeader, "<meta name=""NTBMeldingsSign""", "<meta name=""NTBTjeneste"" content=""NPKTjenesten""/>" & vbCrLf & "<meta name=""NTBMeldingsSign""")
        ElseIf mappe = "Ut-NPKDirekte" Then
            strHeader = Replace(strHeader, "<meta name=""NTBMeldingsSign""", "<meta name=""NTBTjeneste"" content=""NPKDirektetjenesten""/>" & vbCrLf & "<meta name=""NTBMeldingsSign""")
        ElseIf mappe = "Ut-Videotjeneste" Then
            strHeader = Replace(strHeader, "<meta name=""NTBMeldingsSign""", "<meta name=""NTBTjeneste"" content=""Videotjenesten""/>" & vbCrLf & "<meta name=""NTBMeldingsSign""")
        ElseIf mappe = "Ut-NyheterFraNorge" Then
            strHeader = Replace(strHeader, "<meta name=""NTBMeldingsSign""", "<meta name=""NTBTjeneste"" content=""NyheterFraNorge""/>" & vbCrLf & "<meta name=""NTBMeldingsSign""")

        ElseIf mappe = "Ut-Tema" Then
            strHeader = Replace(strHeader, "<meta name=""NTBMeldingsSign""", "<meta name=""NTBTjeneste"" content=""NTBTema""/>" & vbCrLf & "<meta name=""NTBMeldingsSign""")

        ElseIf mappe = "lch" Then ' DEBUG!
            strHeader = Replace(strHeader, "<meta name=""NTBMeldingsSign""", "<meta name=""NTBTjeneste"" content=""NTBTema""/>" & vbCrLf & "<meta name=""NTBMeldingsSign""")

        Else
            strHeader = Replace(strHeader, "<meta name=""NTBMeldingsSign""", "<meta name=""NTBTjeneste"" content=""Spesialtjenester""/>" & vbCrLf & "<meta name=""NTBMeldingsSign""")
        End If

        'Map stoffgruppe
        If ug = "PRM-tjenesten" Then
            strHeader = Replace(strHeader, "<meta name=""NTBStoffgruppe"" content=""" & sg & """/>", "<meta name=""NTBStoffgruppe"" content=""PRM-NTB""/>")
            strHeader = Replace(strHeader, "<meta name=""NTBUndergruppe"" content=""" & ug & """/>", "<meta name=""NTBUndergruppe"" content=""Fulltekstmeldinger""/>")
        ElseIf mappe = "Ut-KulturDirekte" Then
            strHeader = Replace(strHeader, "<meta name=""NTBStoffgruppe"" content=""" & sg & """/>", "<meta name=""NTBStoffgruppe"" content=""Showbiz""/>")
        End If

    End Sub

End Class
