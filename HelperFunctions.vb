Imports System.IO

Module HelperFunctions

    Public Const TEXT_LINE As String = "----------------------------------------------------------------------------------"
    Public Const DATE_TIME As String = "yyyy-MM-dd HH:mm:ss"
    Public Const DATE_LOGFILE As String = "yyyy-MM-dd"

    Public myEncoding As Text.Encoding = Text.Encoding.GetEncoding("iso-8859-1")
    Public txtEncoding As Text.Encoding = Text.Encoding.GetEncoding(1252)

    Public Sub WriteErr(ByRef strLogPath As String, ByRef strMessage As String, ByRef e As Exception)
        Dim strLine As String

        On Error Resume Next
        strLine = "Error: " & Format(Now, DATE_TIME) & vbCrLf
        strLine &= strMessage & vbCrLf
        strLine &= e.Message & vbCrLf
        strLine &= e.Source & vbCrLf
        strLine &= e.StackTrace

        Dim strFile As String = strLogPath & "\Error-" & Format(Now, DATE_LOGFILE) & ".log"
        Dim w As New StreamWriter(strFile, True, myEncoding)
        w.WriteLine(strLine)
        w.WriteLine(TEXT_LINE)
        w.Flush()  ' update underlying file
        w.Close()  ' close the writer and underlying file

    End Sub

    Public Sub WriteLog(ByRef strLogPath As String, ByRef strMessage As String)
        Dim strFile As String = strLogPath & "\Log-" & Format(Now, DATE_LOGFILE) & ".log"
        Dim strLine As String

        strLine = Format(Now, DATE_TIME) & ": " & strMessage

        Dim w As StreamWriter = New StreamWriter(strFile, True, myEncoding)
        w.WriteLine(strLine)
        w.Flush()   '  update underlying file
        w.Close()   '  close the writer and underlying file
    End Sub

    Public Sub WriteLogNoDate(ByRef strLogPath As String, ByRef strMessage As String)
        Dim strFile As String = strLogPath & "\Log-" & Format(Now, DATE_LOGFILE) & ".log"
        Dim w As New StreamWriter(strFile, True, myEncoding)
        w.WriteLine(strMessage)
        w.Flush()   '  update underlying file
        w.Close()   '  close the writer and underlying file
    End Sub

    Public Sub WriteFile(ByRef strFileName As String, ByRef strContent As String, Optional ByVal append As Boolean = False, Optional ByVal encode As String = "windows-1252")
        Dim w As New StreamWriter(strFileName, append, Text.Encoding.GetEncoding(encode))
        w.WriteLine(strContent)
        w.Flush()   '  update underlying file
        w.Close()   '  close the writer and underlying file
    End Sub

    Public Function ReadFile(ByVal strFileName As String, Optional ByVal encode As String = "windows-1252") As String
        ' Simple File reader returns File as String
        Dim sr As New StreamReader(strFileName, Text.Encoding.GetEncoding(encode))      ' File.OpenText("log.txt")
        ReadFile = sr.ReadToEnd
        sr.Close()
    End Function

    Public Sub MakePath(ByVal strPath As String)
        'If Not Directory.Exists(strPath) Then
        Directory.CreateDirectory(strPath)
        'End If
    End Sub


    'Public Structure iptcRec
    '    Dim IptcVal As String
    '    Dim IptcCode As String
    '    Dim IptcRefnum As String
    'End Structure

    'Replaces everything between two tags!
    Function BulkReplace(ByRef strDoc As String, ByVal strFind As String, ByVal strFind2 As String, ByVal strReplace As String, Optional ByVal intStart As Integer = 0, Optional ByVal intCount As Integer = -1) As String
        'Replaces everything between two tags!
        Dim i As Integer
        Dim intLastStart As Integer = 0
        Dim intEnd As Integer = 0
        Dim sbTemp As System.Text.StringBuilder = New System.Text.StringBuilder

        If strFind = "" Or strFind2 = "" Then Return strDoc

        ' Allow max 10000 susbstitutions, if set to -1
        If intCount < 0 Then
            intCount = 10000
        End If

        intLastStart = 0

        For i = 1 To intCount
            intStart = strDoc.IndexOf(strFind, intStart)
            If intStart = -1 Then Exit For

            intEnd = strDoc.IndexOf(strFind2, intStart + Len(strFind))
            If intEnd = -1 Then Exit For

            sbTemp.Append(strDoc.Substring(intLastStart, intStart - intLastStart) & strReplace)

            intStart = intEnd + Len(strFind2)
            intLastStart = intStart
        Next

        If intLastStart = 0 Then
            'No string pair found, do nothing
            Return strDoc
        Else
            'Add last part of string
            sbTemp.Append(strDoc.Substring(intLastStart))
            'strDoc = sbTemp.ToString
            Return sbTemp.ToString
        End If
    End Function

    ' Replaces all paired start and end tags!
    Function PairReplace(ByRef strDoc As String, ByVal strFind As String, ByVal strFind2 As String, ByVal strReplace As String, ByVal strReplace2 As String, Optional ByVal intStart As Integer = 0, Optional ByVal intCount As Integer = -1) As String
        Dim i As Integer
        Dim intLastStart As Integer = 0
        Dim intEnd As Integer = 0
        Dim sbTemp As System.Text.StringBuilder = New System.Text.StringBuilder

        If strFind = "" Or strFind2 = "" Then Return strDoc

        ' Allow max 10000 susbstitutions, if set to -1
        If intCount < 0 Then
            intCount = 100000
        End If

        For i = 1 To intCount
            intStart = strDoc.IndexOf(strFind, intStart)
            If intStart = -1 Then Exit For

            intEnd = strDoc.IndexOf(strFind2, intStart + Len(strFind))
            If intEnd = -1 Then Exit For

            sbTemp.Append(strDoc.Substring(intLastStart, intStart - intLastStart) & strReplace & strDoc.Substring(intStart + Len(strFind), intEnd - intStart - Len(strFind)) & strReplace2)

            intStart = intEnd + Len(strFind2)
            intLastStart = intStart
        Next

        If intLastStart = 0 Then
            'No string pair found, do nothing
            Return strDoc
        Else
            'Add last part of string
            sbTemp.Append(strDoc.Substring(intLastStart))
            Return sbTemp.ToString
        End If
    End Function

    'Replaces everything between two tags!
    Function BulkReplace_Old(ByVal strDoc As String, ByVal strFind As String, ByVal strFind2 As String, ByVal strReplace As String, ByVal intStart As Integer, ByVal intCount As Integer) As String
        'Old function with too many string to string copyings
        'Replaces everything between two tags!
        Dim i As Integer
        Dim lpStart As Integer
        Dim lpSlutt As Integer
        Dim intToCount As Integer
        Dim strTemp As String

        If strFind = "" Or strFind2 = "" Then Exit Function

        If intCount < 0 Then
            intToCount = 10000
        Else
            intToCount = intCount
        End If

        lpStart = intStart

        For i = 1 To intToCount
            lpStart = strDoc.IndexOf(strFind, lpStart)
            If lpStart = -1 Then Exit For
            lpSlutt = strDoc.IndexOf(strFind2, lpStart + Len(strFind))
            If lpSlutt = -1 Then Exit For
            strDoc = strDoc.Substring(0, lpStart) & strReplace & strDoc.Substring(lpSlutt + Len(strFind2))
        Next

        Return strDoc

    End Function

    ' Replaces all paired start and end tags!
    Function PairReplace_old(ByVal strDoc As String, ByVal strFind As String, ByVal strFind2 As String, ByVal strReplace As String, ByVal strReplace2 As String, ByVal intStart As Integer, ByVal intCount As Integer) As String
        Dim i As Integer
        Dim lpStart As Integer
        Dim lpSlutt As Integer
        Dim intToCount As Integer

        If strFind = "" Or strFind2 = "" Then Exit Function

        If intCount < 0 Then
            intToCount = 100000
        Else
            intToCount = intCount
        End If

        lpStart = intStart

        For i = 1 To intToCount
            lpStart = strDoc.IndexOf(strFind, lpStart)
            If lpStart = -1 Then Exit For

            lpSlutt = strDoc.IndexOf(strFind2, lpStart + Len(strFind))
            If lpSlutt = -1 Then Exit For

            strDoc = strDoc.Substring(0, lpStart) & strReplace & strDoc.Substring(lpStart + Len(strFind), lpSlutt - lpStart - Len(strFind)) & strReplace2 & strDoc.Substring(lpSlutt + Len(strFind2))
            lpStart = lpSlutt
        Next

        Return strDoc
    End Function


    Function GetStringPortion(ByRef strString As String, ByVal strStartTag As String, ByVal strEndTag As String) As String
        ' Find Portion of a text-string found between two tags (substrings)
        ' If not both tags are found the function returns an empty string

        Dim intStart, intEnd As Integer

        intStart = strString.IndexOf(strStartTag)
        If intStart = -1 Then
            Return ""
        End If
        intStart += strStartTag.Length

        intEnd = strString.IndexOf(strEndTag, intStart)
        If intEnd = -1 Then
            Return ""
        End If

        Return strString.Substring(intStart, intEnd - (intStart))
    End Function

End Module
