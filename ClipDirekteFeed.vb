Imports System.IO

Public Class ClipDirekteFeed

    Private xsltClipper As MSXML2.IXSLProcessor


    Public Sub New(ByVal clipXSLTFile As String)

        Dim xsltDoc As MSXML2.FreeThreadedDOMDocument
        Dim xmlError As MSXML2.IXMLDOMParseError
        Dim xslt As MSXML2.XSLTemplate

        xslt = New MSXML2.XSLTemplate
        xsltDoc = New MSXML2.FreeThreadedDOMDocument

        xsltDoc.load(clipXSLTFile)
        xmlError = xsltDoc.parseError

        If (xmlError.errorCode <> 0) Then
            'MsgBox(xmlError.reason & " i linje: " & xmlError.line)
        End If

        xslt.stylesheet = xsltDoc
        xsltClipper = xslt.createProcessor

    End Sub

    Function ClipNITFDocument(ByVal input As String, ByVal filename As String) As String

        Dim xml4Doc As MSXML2.DOMDocument = New MSXML2.DOMDocument

        xml4Doc.loadXML(input)
        xsltClipper.input = xml4Doc

        xsltClipper.addParameter("filename", Path.GetFileName(filename))
        xsltClipper.transform()

        Dim ret As String = xsltClipper.output

        'Copyright mark fallback
        If ret.IndexOf("(�NTB)") = -1 Then
            ret = ret.Replace("</body.content>", "<p class=""txt"">(�NTB)</p>" & vbCrLf & "</body.content>")
        End If

        Return ret


    End Function

End Class
