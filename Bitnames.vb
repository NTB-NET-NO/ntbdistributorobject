Imports System.Data.SqlClient
Imports System.Xml
'Imports System.Text.RegularExpressions

Public Class Bitnames

    Public Structure iptcRec
        Dim IptcVal As String
        Dim IptcCode As String
        Dim IptcRefnum As String
    End Structure

    Enum htType As Integer
        Category = 3
        SubCategory = 4
        Evloc = 5
        County = 6
    End Enum

    Private SqlDataAdapter1 As SqlDataAdapter
    Private SqlSelectCommand1 As SqlCommand
    Private SqlConnection1 As SqlConnection
    Private DataSet1 As Data.DataSet

    Private htCategories As Hashtable = New Hashtable()

    'Functions for Initialisering
    Public Sub New(ByVal strConnectionString As String, ByVal BitnameOfflineFile As String)
        SqlDataAdapter1 = New System.Data.SqlClient.SqlDataAdapter()
        SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand()
        SqlConnection1 = New System.Data.SqlClient.SqlConnection()
        DataSet1 = New Data.DataSet()

        'SqlDataAdapter1
        SqlDataAdapter1.SelectCommand = Me.SqlSelectCommand1

        'SqlSelectCommand1
        SqlSelectCommand1.CommandText = "[GetBitnamesForNitf]"
        SqlSelectCommand1.CommandType = System.Data.CommandType.StoredProcedure
        SqlSelectCommand1.Connection = Me.SqlConnection1

        'DataSet11
        DataSet1.DataSetName = "DataSet1"
        'Me.DataSet1.Locale = New System.Globalization.CultureInfo("nb-NO")
        'Me.DataSet1.Namespace = "http://www.tempuri.org/DataSet1.xsd"
        'CType(Me.DataSet1, System.ComponentModel.ISupportInitialize).EndInit()

        LoadDataset(strConnectionString, BitnameOfflineFile)
        FillAllHashtables()
    End Sub

    Public Sub LoadDataset(ByVal strConnectionString As String, ByVal BitnameOfflineFile As String)

        SqlConnection1.ConnectionString = strConnectionString

        Try
            SqlConnection1.Open()
            SqlDataAdapter1.Fill(DataSet1)
            SqlConnection1.Close()
            DataSet1.WriteXml(BitnameOfflineFile)
        Catch
            SqlConnection1.Close()
            DataSet1.ReadXml(BitnameOfflineFile)
        End Try
    End Sub

    Private Sub FillAllHashtables()
        FillHashtable("TypeOfName=3", htType.Category)
        FillHashtable("TypeOfName=4", htType.SubCategory)
        FillHashtable("TypeOfName=5", htType.Evloc)
        FillHashtable("TypeOfName=6", htType.County)
    End Sub

    Private Sub FillHashtable(ByVal strQuery As String, ByVal typeHash As htType)
        '*** Fill Bitmaps from table BITNAMES: for Category and Subcategory into Hastables
        'htBitname = New Hashtable()
        Dim strKey As String
        Dim iptc As iptcRec
        Dim i As Integer
        Dim strElement As String
        Dim intCategory As Integer
        Dim row As DataRow
        Dim dvBitnames As DataView = New DataView(DataSet1.Tables(0), strQuery, "", DataViewRowState.CurrentRows)

        For i = 0 To dvBitnames.Count - 1
            row = dvBitnames(i).Row
            strKey = LCase(row("NotabeneVal"))

            iptc.IptcCode = row("IptcCode") & ""
            iptc.IptcRefnum = row("IptcRefnum") & ""
            iptc.IptcVal = row("IptcVal") & ""

            Select Case typeHash
                Case htType.Category, htType.SubCategory
                    strElement = MakeXmlTobject(iptc, typeHash)
                Case htType.Evloc
                    strElement = MakeXmlEvloc(iptc)
                Case htType.County
                    strElement = MakeXmlCounty(iptc)
            End Select

            htCategories.Add(typeHash & LCase(strKey), strElement)

        Next

        dvBitnames.Dispose()

    End Sub

    Private Function MakeXmlTobject(ByRef iptc As iptcRec, ByVal typeHash As htType) As String
        Dim strElement As String = "<tobject.subject"
        If iptc.IptcCode <> "" Then
            strElement &= " tobject.subject.code='" & iptc.IptcCode & "'"
        End If
        If iptc.IptcRefnum <> "" Then
            strElement &= " tobject.subject.refnum='" & iptc.IptcRefnum & "'"
        End If
        If iptc.IptcVal <> "" Then
            If typeHash = htType.Category Then
                strElement &= " tobject.subject.type='" & iptc.IptcVal & "'"
            Else
                strElement &= " tobject.subject.matter='" & iptc.IptcVal & "'"
            End If
        End If
        strElement &= "/>"

        Return strElement
    End Function

    Private Function MakeXmlEvloc(ByRef iptc As iptcRec) As String
        Return "<evloc state-prov='" & iptc.IptcVal & "'/>"
    End Function

    Private Function MakeXmlCounty(ByRef iptc As iptcRec) As String
        Return "<evloc state-prov='Norge' county-dist='" & iptc.IptcVal & "'/>"
    End Function

    'Public functions for RunTime
    Public Function MakeNotaben2NitfFields(ByRef strXml As String) As XmlDocument
        Dim xmlHead As XmlDocument = New XmlDocument()

        xmlHead.LoadXml(strXml)

        AddNitfNodes(xmlHead, "NTBHovedKategorier", Bitnames.htType.Category)
        AddNitfNodes(xmlHead, "NTBUnderKatKultur", Bitnames.htType.SubCategory)
        AddNitfNodes(xmlHead, "NTBUnderKatOkonomi", Bitnames.htType.SubCategory)
        AddNitfNodes(xmlHead, "NTBUnderKatKuriosa", Bitnames.htType.SubCategory)
        AddNitfNodes(xmlHead, "NTBUnderKatSport", Bitnames.htType.SubCategory)
        AddNitfNodes(xmlHead, "NTBUnderKatFritid", Bitnames.htType.SubCategory)

        AddNitfNodes(xmlHead, "NTBOmraader", Bitnames.htType.Evloc)
        AddNitfNodes(xmlHead, "NTBFylker", Bitnames.htType.County)

        Return xmlHead
    End Function

    Private Sub AddNitfNodes(ByRef xmlHead As XmlDocument, ByVal strNotabeneField As String, ByVal typeHash As Bitnames.htType)
        Dim strFields As String
        Dim arrFields As String()

        Try
            strFields = xmlHead.SelectSingleNode("//meta[@name='" & strNotabeneField & "']/@content").InnerText
        Catch
            Return
        End Try

        arrFields = Split(strFields, ";")
        Array.Sort(arrFields)
        Dim field, lastField As String
        For Each field In arrFields
            'hopper over tomme og like verdier
            If field <> "" And field <> lastField Then
                AddTobject(xmlHead, field, typeHash)
            End If
            lastField = field
        Next field

    End Sub

    Private Sub AddTobject(ByRef xmlHead As XmlDocument, ByVal strField As String, ByVal typeHash As Bitnames.htType)
        Dim strElement As String
        'strElement = GetNitfElement(strField, typeHash)
        strElement = htCategories.Item(typeHash & LCase(strField))

        If strElement Is Nothing Then
            strElement = strField
        End If

        'Create a new node.
        Dim elem As XmlElement = xmlHead.CreateElement("nitf-elem")
        elem.InnerXml = strElement

        'Add the node to the document.
        Dim root As XmlNode = xmlHead.DocumentElement
        root.AppendChild(elem)
        'Debug.WriteLine(xmlHead.InnerXml())
    End Sub

    Private Function GetNitfElement(ByRef strKey As String, ByVal typeHash As htType) As String
        Dim strElement As String
        strElement = htCategories.Item(typeHash & LCase(strKey))
        Return strElement
    End Function

    Protected Sub Close()
        SqlConnection1.Close()
        SqlConnection1.Dispose()
        DataSet1.Dispose()
    End Sub
End Class
